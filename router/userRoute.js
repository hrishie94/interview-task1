const express = require("express")
const router = express.Router()
const userController = require("../controller/userController")
const auth = require("../middleware/auth")
const  { userValidationRules,validate} = require("../middleware/validator")
const cache = require("../middleware/cache")

router.post("/createUser", userValidationRules(),validate, userController.create)

router.post("/getUser", auth, userController.get)

router.get("/getAllUser", auth, cache(300), userController.getall)

router.post("/updateUser", auth, userController.update)

router.post("/deleteUser", auth, userController.delete)



module.exports = router