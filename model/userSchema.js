const mongoose = require("mongoose")
const Schema = mongoose.Schema

const userSchema = new Schema({
  first_name: { type: String, default: require },
  last_name: { type: String, default: require },
  address: { type: String, default: require },
  mobileNumber: { type: Number, default: require },
  email: { type: String, default: require },
  password: { type: String ,default: require},
  token: { type: String }
});

module.exports = mongoose.model("user", userSchema)