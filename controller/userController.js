const userController = {}
const user = require("../model/userSchema")
const bcrypt = require("bcryptjs")
require("dotenv").config()
const jwt = require("jsonwebtoken")
userController.create = async (req, res) => {
    try {
        const data = req.body
        let existedEmail = await user.findOne({ email: req.body.email })
        if (data.email === existedEmail) {
            return res.status(403).send({ "message": "this email already existedEmail, Please try with new one " })
        }
        encryptedPassword = await bcrypt.hash(data.password, 10);
        data.password = encryptedPassword
        data.email = data.email.toLowerCase()
        console.log(data, "data")
        email = data.email
        const token = jwt.sign(
            { user_id: user._id, email },
            process.env.TOKEN_KEY,
            {
                expiresIn: "2h",
            }
        );
        console.log(token, "token")
        // save user token
        //   data.token = token;

        const userData = new user(data)
        await userData.save()
        console.log(userData)
        userData.token = token;

        return res.status(200).json(userData)
    } catch (error) {
        return res.status(400).send({ message: error.message })
    }
}


userController.get = async (req, res) => {
    try {
        const data = req.body
        const alldata = await user.find({ _id: data._id })
        console.log(alldata)
        if(alldata.length){
            return res.status(200).json(alldata)
        }else{
        return res.status(200).json({"message":"the User does not exist"})
        }
    } catch (error) {
        return res.status(400).send({ message: error.message })
    }
}

userController.getall = async (req, res) => {
    try {
        const alldata = await user.find({})

        return res.status(201).json(alldata)

        // console.log(alldata)
    } catch (error) {
        return res.status(400).send({ message: error.message })
    }
}



userController.update = async (req, res) => {
    try {
        const data = req.body
        const _id = req.body._id
        const alldata = await user.updateOne({ _id },
            { $set: req.body }, {
            new: true
        })
        return res.status(200).json({ message: "the user has been updated" })
    } catch (error) {
        return res.status(400).send({ message: error.message })
    }
}



userController.delete = async (req, res) => {
    try {
        const alldata = await user.deleteOne({ _id: req.body._id })
        return res.status(200).send({ message: "the user has been removed" })
    } catch (error) {
        return res.status(400).send({ message: error.message })
    }
}

module.exports = userController