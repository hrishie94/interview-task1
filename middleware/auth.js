const jwt = require("jsonwebtoken");

const config = process.env.TOKEN_KEY;

const verifyToken = async (req, res, next) => {
  // console.log(req,res,"v")

  // req.token = bearerToken

  try {

    const token = req.body.token || req.query.token || req.headers["authorization"];
    // console.log(token,"token")
    const bearer = token.split(" ")
    const bearerToken = bearer[1]
    if (!token) {
      return res.status(401).send("A token is required for authentication");
    }
    // console.log(bearerToken)
    // console.log(config)

    const user = await new Promise((resolve, reject) => {
      jwt.verify(bearerToken, config, (err, user) => {
        if (err) reject({ message: 'token expired', code: 401 })
        resolve(user)
      });
    })
    req.user = user

  } catch (err) {
    return res.status(401).send("Invalid Token");
  }
  return next();
};

module.exports = verifyToken;