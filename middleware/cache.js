const NodeCache = require("node-cache");
const myCache = new NodeCache();

module.exports = duration => (req, res, next) => {
    if (req.method !== "GET") {
        console.error("request can not complete non GET methods")
        return next()
    }
    let key = req.originalUrl
    const cachedResponse = myCache.get(key)
    console.log("cache  ", key)

    if (cachedResponse) {
        console.log("cache hit for ", key)
        res.send(cachedResponse)
    } else {
        console.log("cache miss for ", key)
        // console.log("cache miss for ",(res.send))

        res.originalSend = res.send
        res.send = body => {
            res.originalSend(body)
            myCache.set(key, body, duration)
        }
    }
    next()
}