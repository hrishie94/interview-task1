const { body, validationResult } = require('express-validator')
const userValidationRules = () => {
  return [
    body("first_name").notEmpty().isString(),
    body("last_name").notEmpty().isString(),
    body("address").notEmpty().isString(),
    body("mobileNumber").notEmpty().isNumeric(),
    body('email').notEmpty().isEmail(),
    body('password').notEmpty().isLength({ min: 5,max:8 }),
  ]
}
console.log(userValidationRules,"val")
const validate = (req, res, next) => {
  const errors = validationResult(req.body)
  if (errors.isEmpty()) {
    return next()
  }
  const extractedErrors = []
  errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }))

  return res.status(422).json({
    errors: extractedErrors,
  })
}

module.exports = {
  userValidationRules,
  validate,
}