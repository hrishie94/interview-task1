const express = require("express")
require("dotenv").config()
const app = express();
app.use(express.json())
const port = process.env.port
const connectDB = require("./db/connection");
connectDB()

const userRouter = require("./router/userRoute")
app.use("/", userRouter)

app.listen(port, () => {
    console.log("the server has been started", port)
})